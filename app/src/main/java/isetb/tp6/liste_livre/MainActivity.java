package isetb.tp6.liste_livre;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView list;
    String [] nom={"LA DEMOISELLE","L'ENFANT DE LUMIERE","SORTIR DES EMOTIONS NEGATIVES","CULTIVEZ L'ENERGIE POSITIVE","HTML5 et CSS3","Je crée mon site avec WordPress","Guide critique de l'évolution"};
    String [] detaille={"Marie de Palet\n" +
            "EAN : 9782844941565\n" +
            "318 pages\n" +
            "Éditeur : EDITIONS DE BORÉE (10/06/2003)","Daniel Dupuy\n" +
            "EAN : 9782812927355\n" +
            "Éditeur : EDITIONS DE BORÉE (11/11/2021)","Jean Cottraux\n" +
            "EAN : 9782738153210\n" +
            "291 pages\n" +
            "Éditeur : ODILE JACOB (07/04/2021)","Vex King\n" +
            "\n" +
            "Raphaëlle Giordano (Autre)\n" +
            "EAN : 9782266314893\n" +
            "288 pages\n" +
            "Éditeur : POCKET (17/06/2021)","Christophe Aubry\n" +
            "EAN : 9782746085169\n" +
            "501 pages\n" +
            "Éditeur : ENI (11/12/2013)\n" +
            "\n" +
            "Note moyenne : /5 (sur 0 notes)","Lycia Diaz\n" +
            "EAN : 9782416005114\n" +
            "Éditeur : EYROLLES (24/02/2022)\n" +
            "\n" +
            "Note moyenne : /5 (sur 0 notes)","Guillaume Lecointre\n" +
            "Marie-Laure Le Louarn-Bonnet\n" +
            "Corinne Fortin\n" +
            "Gérard Guillot\n" +
            "EAN : 9782701147970\n" +
            "571 pages\n" +
            "Éditeur : EDITIONS BELIN (03/11/2009)\n" +
            "4.53/5   19 notes"};
    Integer [] portrait={R.drawable.dmm,R.drawable.dd,R.drawable.emotion,R.drawable.energi,R.drawable.html,R.drawable.cree,R.drawable.guid};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list=findViewById(R.id.l);
        SystemExAdapter adapter=new SystemExAdapter(this,portrait,nom,detaille);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int info, long id) {
                AlertDialog.Builder a= new AlertDialog.Builder(MainActivity.this);
                a.setTitle("info livre");
                a.setIcon(portrait[info]);
                a.setMessage(nom[info]+"------->"+detaille[info]);
                a.setPositiveButton("ok",null);
                a.show();
            }
        });

    }
}